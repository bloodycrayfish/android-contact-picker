package com.example.autocompletechips;

public class ContactItem {
    private String mContactName;
    private String mContactEmail;
    private String mContactPhone;

    public ContactItem(String contactName, String contactEmail) {
        mContactName = contactName;
        mContactEmail = contactEmail;
        mContactPhone = "";
    }

    public ContactItem(String contactName, String contactEmail, String contactPhone) {
        mContactName = contactName;
        mContactEmail = contactEmail;
        mContactPhone = contactPhone;
    }

    public String getContactEmail() {
        return mContactEmail;
    }

    public String getContactName() {
        return mContactName;
    }

    public String getmContactPhone() {
        return mContactPhone;
    }
}
