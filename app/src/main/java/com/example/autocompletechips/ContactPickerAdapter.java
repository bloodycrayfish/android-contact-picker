package com.example.autocompletechips;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ContactPickerAdapter extends ArrayAdapter<ContactItem> {

    private List<ContactItem> mContactListFull;
    private ContactType mContactTypeToComplete;

    public enum ContactType {
        PHONE,
        EMAIL
    }

    public ContactPickerAdapter(Context context, List<ContactItem> contactList,ContactType contactType) {
        super(context, 0, contactList);
        mContactListFull = new ArrayList<>(contactList);
        mContactTypeToComplete = contactType;
    }

    @Override
    public Filter getFilter() {
        return mContactFiler;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.contact_autocomplete_row, parent, false
            );
        }

        TextView textViewName = convertView.findViewById(R.id.contact_name);
        TextView textViewEmail = convertView.findViewById(R.id.contact_email);

        ContactItem item = getItem(position);

        if (item != null) {
            textViewName.setText(item.getContactName());
            textViewEmail.setText(item.getContactEmail());
        }
        return convertView;
    }

    private Filter mContactFiler = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();
            List<ContactItem> suggestions = new ArrayList<>();

            if (charSequence == null || charSequence.length() == 0) {
                suggestions.addAll(mContactListFull);
            } else {
                String filterPattern = charSequence.toString().toLowerCase().trim();
                for (ContactItem item : mContactListFull) {
                    if ((item.getContactName().toLowerCase().contains(filterPattern))
                            || (item.getContactEmail().toLowerCase().contains(filterPattern))
                            || (item.getmContactPhone().toLowerCase().contains(filterPattern))) {
                        suggestions.add(item);
                    }
                }
            }
            //could sort the suggestions here
            results.values = suggestions;
            results.count = suggestions.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            clear();
            addAll((List) filterResults.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((ContactItem) resultValue).getContactEmail();
        }
    };
}
