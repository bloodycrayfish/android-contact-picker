package com.example.autocompletechips;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.text.Editable;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.chip.Chip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private List<ContactItem> mContactItems;

    private final static String[] CONTACT_NAMES = new String[]{
            "Arthur", "Bacon", "Ben", "Bob", "Bender Cucumber", "Crepes", "Pancakes", "Waffels", "Regina Rosenberry", "Eva Evanoff", "Rosann Robin", "Ebonie Eady", "Mia Mathieson", "Reda Reta", "Corrina Coplan", "Raymonde Rushing", "Shawanda Strandberg", "Lurlene Lindsay", "Blanch Byas", "Cheryl Cassinelli", "Dacia Dauenhauer", "Felipe Ferrante", "Danille Duby", "Jarrod Joplin", "Amira Amor", "Grazyna Grasso", "Lakisha Luebbers", "Eden Eberhard", "Twanna Townsend", "Bobby Bosque", "Arla Ashton", "Ona Oram", "Diego Deets", "Geralyn Gladney", "Leticia Lembke", "Casey Coberly", "Bula Branscum", "Aliza Aliff", "Lou Loiacono", "Lulu Lowman", "Pauline Parisien", "Minda Mosser", "Kara Kort", "Sook Suder", "Eloy Edgemon", "Stanton Strachan", "Lisette Lampley", "Echo Etchison", "Donna Degregorio", "Janey Jacobs", "Shondra Southers", "Leonor Leff", "Euna Edmonson", "Alonzo Almquist", "Eura Ebright", "Rashida Rikard", "Antwan Alphonse", "Clay Clair"
    };

    private final static String[] CONTACT_EMAILS = new String[]{
            "big_sword@yahoo.co.za", "zzzzzooom@me.com", "Ben@hotmail.com", "me@Bob.com", "cucumberpatch@google.com", "", "me@me", "OHNOEY@bacon.syrup", "omcglynn@lebsack.com", "kris30@stokes.com", "upton.merlin@stehr.com", "eddie02@hotmail.com", "armando56@hotmail.com", "jdach@gmail.com", "alene.rogahn@leannon.com", "jpollich@abernathy.com", "halvorson.berta@hill.com", "mitchell.arianna@corkery.com", "tod.schulist@schowalter.biz", "mark02@yahoo.com", "labadie.ubaldo@herzog.com", "kessler.payton@hotmail.com", "madilyn46@weimann.com", "genoveva56@nitzsche.info", "rgutkowski@yahoo.com", "baumbach.mohammed@klein.com", "ferry.valentine@hamill.com", "bgislason@hotmail.com", "omurazik@hotmail.com", "lubowitz.imani@gmail.com", "padberg.annabel@hotmail.com", "savanah.dickinson@shanahan.com", "maurice73@tromp.info", "carlotta85@walter.biz", "ncarter@gmail.com", "ukoepp@pfeffer.info", "karianne30@gmail.com", "jamaal.torphy@yahoo.com", "pfranecki@hotmail.com", "casper.eddie@hotmail.com", "walker.marion@gmail.com", "michale.schiller@vonrueden.org", "jacobi.emilie@gleichner.com", "maxwell56@gmail.com", "lehner.keira@hotmail.com", "murazik.malika@waelchi.com", "mallie13@gmail.com", "jazmyne.kutch@gmail.com", "ryan.amiya@gmail.com", "jerald.bartoletti@gmail.com", "paul65@yahoo.com", "borer.lupe@hessel.com", "cruickshank.verda@yahoo.com", "marco59@hotmail.com", "lina.zemlak@hotmail.com", "stracke.ashton@gmail.com", "vickie.shanahan@fritsch.biz", "lboehm@gmail.com"
    };

    private ContactAutocompleteView mContactPicker;
    private Button mDisplayEmailsButton;
    private TextView mDisplayEmailsView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fillContactList();

        //need to explicitly set the contacts on the contactpicker to activate the
        // autocomplete functionality
        mContactPicker = findViewById(R.id.contact_field);
        mContactPicker.setContacts(mContactItems);

        mDisplayEmailsButton = findViewById(R.id.display_emails_button);
        mDisplayEmailsView = findViewById(R.id.emails_view);

        mDisplayEmailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailList = "";
                List<String> contactEmails = mContactPicker.getContactEmails();
                for(String email : contactEmails){
                    emailList = emailList + email +", ";
                }
                emailList = emailList.replaceAll(", $", "");
                mDisplayEmailsView.setText(emailList);
            }
        });
    }

    private void fillContactList() {
        mContactItems = new ArrayList<>();
        for (int i = 0; i < CONTACT_EMAILS.length - 1; i++) {
            mContactItems.add(new ContactItem(CONTACT_NAMES[i], CONTACT_EMAILS[i]));
        }
    }

}
