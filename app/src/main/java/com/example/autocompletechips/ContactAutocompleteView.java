package com.example.autocompletechips;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.android.material.chip.Chip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactAutocompleteView extends FlowLayout {

    private Context mContext;

    private AutoCompleteTextView mContactPicker;

    private Map<Integer, ContactItem> mContactsFilledIn = new HashMap();
    private Integer mChipCounter = 0;

    public ContactAutocompleteView(Context context) {
        super(context);
        mContext = context;

        inflate(context, R.layout.contact_picker_layout, this);

        initContactPicker();
    }

    public ContactAutocompleteView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mContext = context;

        inflate(context, R.layout.contact_picker_layout, this);

        initContactPicker();
    }

    public void initContactPicker() {

        mContactPicker = findViewById(R.id.contact_picker);

        //this converts the autocomplete item selection to a chip and clears the Autocompletetextview
        mContactPicker.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactItem selected = (ContactItem) parent.getItemAtPosition(position);
                addContactAndMapToChip(selected);
                mContactPicker.setText("");
            }
        });

        // this listener handles when the user clicks 'DONE' or backspace (while the textview is empty)
        mContactPicker.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                boolean handled = false;
                String value = ((AutoCompleteTextView) view).getText().toString();
                if ((keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)
                            && (keyEvent.getAction()==0)) {
                    handled = validateAndAddContact(value);
                } else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DEL && keyEvent.getAction()==0) {
                    if (value.isEmpty()) {
                        if (getChildCount() > 1) {
                            ContactItem contact = getLastChipContactAndRemoveChip();
                            mContactPicker.setText(contact.getContactEmail());
                            mContactPicker.setSelection(mContactPicker.getText().length());
                            handled = true;
                        }
                    }
                }
                return handled;
            }
        });

        //this listener converts the input to a chip if the user types ',' or ';' or ' ' i.e. typically
        // used to indigate that the user wants to start typing a new contact
        mContactPicker.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //do nothing
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    Character c = charSequence.charAt(charSequence.length() - 1);
                    if ((c == ',') || (c == ';') || (c == ' ')) {
                        String contact = "";
                        if (charSequence.length() > 1) {
                            contact = charSequence.subSequence(0, charSequence.length() - 1).toString();
                        }
                        boolean handled = validateAndAddContact(contact);
                        if (!handled) {
                            mContactPicker.setText(contact);
                            mContactPicker.setSelection(contact.length());
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //do nothing
            }
        });
    }

    public void setContacts(List<ContactItem> contacts) {

        ContactPickerAdapter adapter = new ContactPickerAdapter(mContext, contacts, ContactPickerAdapter.ContactType.EMAIL);

        mContactPicker.setAdapter(adapter);
    }

    private void addContactAndMapToChip(ContactItem contact) {
        //create the chip
        final Chip chip = new Chip(mContext);
        chip.setId(mChipCounter);
        chip.setText(contact.getContactName());
        chip.setChipIcon(ContextCompat.getDrawable(mContext, R.drawable.ic_person));
        chip.setCloseIconVisible(true);
        chip.setClickable(true);
        chip.setCheckable(false);

        // add chip just before input and map it
        int childrenCount = getChildCount();
        addView(chip, childrenCount - 1);
        mContactsFilledIn.put(mChipCounter, contact);
        mChipCounter += 1;

        //on remove also remove from list
        chip.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeView(chip);
                mContactsFilledIn.remove(chip.getId());
            }
        });
    }

    private ContactItem getLastChipContactAndRemoveChip() {
        if (getChildCount() > 1) {
            Chip lastContactChip = (Chip) getChildAt(getChildCount() - 2);

            ContactItem contactItem = mContactsFilledIn.get(lastContactChip.getId());

            removeView(lastContactChip);
            mContactsFilledIn.remove(lastContactChip.getId());

            return contactItem;
        }
        return null;
    }

    private boolean validateAndAddContact(String contact) {
        boolean handled = false;
        if (!contact.isEmpty() && isValidEmail(contact)) {
            ContactItem entered;
            entered = new ContactItem(contact, contact);
            mContactPicker.setText("");
            addContactAndMapToChip(entered);
            handled = true;
        } else {
            Toast.makeText(mContext, "Invalid email address/phone number", Toast.LENGTH_LONG).show();
        }
        return handled;
    }

    public List<String> getContactEmails() {
        List<String> contactEmails = new ArrayList<>();
        for (ContactItem contact : mContactsFilledIn.values()) {
            contactEmails.add(contact.getContactEmail());
        }
        return contactEmails;
    }

    public List<String> getContactPhoneNumbers() {
        List<String> contactPhoneNumbers = new ArrayList<>();
        for (ContactItem contact : mContactsFilledIn.values()) {
            contactPhoneNumbers.add(contact.getContactEmail());
        }
        return contactPhoneNumbers;
    }

    private static boolean isValidEmail(String email) {
        email = email.trim();
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Matcher matcher = emailPattern.matcher(email);
        return matcher.matches();
    }

    private static boolean isValidPhoneNumber(String number) {
        number = number.trim();
        Pattern emailPattern = Patterns.PHONE;
        Matcher matcher = emailPattern.matcher(number);
        return matcher.matches();
    }
}
